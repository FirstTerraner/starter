import { IMoveParams } from '../models/player'
import { canvas } from './Canvas'
import { events } from './Events'
import { getMap } from './Movables'
import { Sprite } from './Sprite'

export class Player {

	static #instance: Player
	#sprite

	/** @returns the player sprite object */
	get sprite() { return this.#sprite }

	public static newPlayer() {
		if (!this.#instance) {
			return new Player()
		}
		return this.#instance
	}

	constructor() {
		const playerImgWidth = 579
		const playerImgHeight = 54
		const framesCount = 9
		this.#sprite = new Sprite({
			position: {
				x: canvas.element.width / 2 - playerImgWidth / framesCount / 2,
				y: canvas.element.height / 2 - playerImgHeight / 2
			},
			image: { src: './imgs/players/standard/down.png' },
			frames: { max: 9, hold: 5 },
			sprites: {
				up: { src: './imgs/players/standard/up.png' },
				right: { src: './imgs/players/standard/right.png' },
				down: { src: './imgs/players/standard/down.png' },
				left: { src: './imgs/players/standard/left.png' }
			},
		})
	}

	public move = ({ moving, animationID, inHouse }: IMoveParams) => {
		if (events.keys.w.pressed && events.lastKeyPressed === 'w') {
			this.#handleSpriteDirection({ direction: 'up' })
			if (moving) {
				// move the movables depending on user input
				getMap().moveMovables('w')
				// update offset
				// getMap().updateMapOffset()
			}
			return
		}
		if (events.keys.a.pressed && events.lastKeyPressed === 'a') {
			this.#handleSpriteDirection({ direction: 'left' })
			if (moving) {
				// move the movables depending on user input
				getMap().moveMovables('a')
				// update offset
				// getMap().updateMapOffset()
			}
			return
		}
		if (events.keys.s.pressed && events.lastKeyPressed === 's') {
			this.#handleSpriteDirection({ direction: 'down' })
			if (moving) {
				// move the movables depending on user input
				getMap().moveMovables('s')
				// update offset
				// getMap().updateMapOffset()
			}
			return
		}
		if (events.keys.d.pressed && events.lastKeyPressed === 'd') {
			this.#handleSpriteDirection({ direction: 'right' })
			if (moving) {
				// move the movables depending on user input
				getMap().moveMovables('d')
				// update offset
				// getMap().updateMapOffset()
			}
			return
		}
		// reset player sprite frame to 0 if standing still
		this.sprite.frames.val = 0
	}

	#handleSpriteDirection({ direction }: { direction: 'up' | 'right' | 'down' | 'left' }) {
		this.sprite.animate = true
		this.sprite.image = this.sprite.sprites[direction]
	}

}