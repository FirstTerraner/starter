
import { player } from '..'
import { IMovablesArgs } from '../models/movables'
import { canvas } from './Canvas'
import { Sprite } from './Sprite'

class Movables {

	#mapID
	#houseID
	#animationID
	#background
	// #foreground
	#movingByPixel
	#movables

	public constructor({ mapID, houseID }: IMovablesArgs) {
		this.#mapID = mapID
		this.#houseID = houseID || 0
		this.#animationID = 0
		this.#background = new Sprite({
			position: { x: 0, y: 0 },
			image: { src: `./imgs/maps/map${mapID}/${houseID ? `houses/house${houseID}` : ''}/background.png` }
		})
		this.#movingByPixel = 2.5
		this.#movables = [
			this.#background
		]
	}

	public animate = () => {
		// console.log({ animationID: this.#animationID })
		this.#animationID = requestAnimationFrame(this.animate)
		// clear canvas
		canvas.ctx?.clearRect(0, 0, canvas.element.width, canvas.element.height)
		// start new Path
		canvas.ctx?.beginPath()
		// render map background
		this.#background.draw()
		// render player
		player.sprite.draw()
		const moving = true
		player.sprite.animate = false
		player.move({ moving })
	}

	public moveMovables(direction: 'w' | 'a' | 's' | 'd') {
		for (let i = 0; i < this.#movables.length; i++) {
			const movable = this.#movables[i]
			if (movable) {
				if (direction === 'w') {
					if (movable instanceof Sprite) {
						movable.position.y += this.#movingByPixel
					}
				}
				if (direction === 'a') {
					if (movable instanceof Sprite) {
						movable.position.x += this.#movingByPixel
					}

				}
				if (direction === 's') {
					if (movable instanceof Sprite) {
						movable.position.y -= this.#movingByPixel
					}

				}
				if (direction === 'd') {
					if (movable instanceof Sprite) {
						movable.position.x -= this.#movingByPixel
					}
				}
			}
		}
	}
}

let map = new Movables({ mapID: 20, houseID: 0 })

export function getMap() { return map }

export function setMap({ mapID, houseID }: IMovablesArgs) { map = new Movables({ mapID, houseID }) }
