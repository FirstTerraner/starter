import { ISpriteArgs } from '../models/sprite'
import { canvas } from './Canvas'
import { Img } from './Img'

export class Sprite {

	image
	#width = 0
	#height = 0
	position
	frames
	sprites
	animate
	rotation
	opacity
	brightness

	constructor({
		image,
		position,
		frames = { max: 1, hold: 10 },
		sprites,
		animate = false,
		rotation = 0,
	}: ISpriteArgs) {
		this.image = new Img(image.src)
		this.image.onload = () => {
			const imgWidth = this.image.width
			const imgHeight = this.image.height
			this.#width = (imgWidth / this.frames.max)
			this.#height = imgHeight
		}
		this.position = position
		this.frames = { ...frames, val: 0, elapsed: 0 }
		this.sprites = {
			up: new Img(sprites?.up.src),
			right: new Img(sprites?.right.src),
			down: new Img(sprites?.down.src),
			left: new Img(sprites?.left.src)
		}
		this.animate = animate
		this.rotation = rotation
		this.opacity = 1
		this.brightness = 100
	}

	public draw() {
		if (!canvas.ctx || !this.#height) { return }
		canvas.ctx.save()
		// 
		canvas.ctx.translate(this.position.x + this.#width / 2, this.position.y + this.#height / 2)
		// for attack-sprites to face the correct direction (accepts range from 0 - 6.2)
		canvas.ctx.rotate(this.rotation)
		//
		canvas.ctx.translate(-this.position.x - this.#width / 2, -this.position.y - this.#height / 2)
		// set opacity in canvas context
		canvas.ctx.globalAlpha = this.opacity
		if (this.brightness !== 100) {
			canvas.ctx.filter = `brightness(${this.brightness}%)`
		}
		// https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage
		canvas.ctx.drawImage(
			this.image,
			this.frames.val * this.#width,
			0,
			this.image.width / this.frames.max,
			this.image.height,
			this.position.x,
			this.position.y,
			this.image.width / this.frames.max,
			this.image.height
		)
		// associated to ctx.save() and ctx.globalAlpha
		canvas.ctx.restore()
		if (!this.animate) { return }
		// set frames elapsed for sprites cropped in 4 pieces, to create move animation 
		if (this.frames.max > 1) {
			this.frames.elapsed++
		}
		// changes the cropped piece with the next one to create move animation
		if (this.frames.elapsed % this.frames.hold === 0) {
			if (this.frames.val < this.frames.max - 1) {
				// npcs with random direction change
				// if (this.image.src.includes('/npcs/') && this.frames.hold == 210) {
				// 	this.frames.val = getRandomInt(0, 3)
				// 	return
				// }
				// rest of sprites with incremental direction change
				this.frames.val++
			}
			else { this.frames.val = 0 }
		}
	}
}