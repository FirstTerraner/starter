import { getElByQuery } from '../utils/html'

class Canvas {

	static #instance: Canvas
	#element
	#ctx

	/***** Getter *****/

	/**@returns the canvas element */
	get element() { return this.#element }
	/**@returns the canvas 2D context API */
	get ctx() {
		if (!this.#ctx) { return }
		return this.#ctx
	}

	public static newCanvas() {
		if (!this.#instance) { return new Canvas() }
		return this.#instance
	}

	private constructor() {
		this.#element = getElByQuery<HTMLCanvasElement>('canvas')
		this.#element.width = this.setSizes().x
		this.#element.height = this.setSizes().y
		this.#ctx = this.#element.getContext('2d', { alpha: false })
		if (!this.#ctx) { return }
		this.#ctx.imageSmoothingEnabled = false
	}

	public setSizes() {
		return { x: window.innerWidth, y: window.innerHeight }
		// if (viewport.isSmartphone()) {
		// 	return {
		// 		x: window.innerWidth,
		// 		y: window.innerHeight / 2.4
		// 	}
		// }
		// if (viewport.isTurnedSmartphone()) {
		// 	return {
		// 		x: window.innerWidth / 1.6,
		// 		y: window.innerHeight
		// 	}
		// }
		// if (viewport.isTablet()) {
		// 	return {
		// 		x: window.innerWidth,
		// 		y: window.innerHeight / 2
		// 	}
		// }
		// if (viewport.isSmallDesktop()) {
		// 	return {
		// 		x: window.innerWidth / 1.3,
		// 		y: window.innerHeight / 1.5
		// 	}
		// }
		// if bigger than small desktop, set canvas to a fixed x & y size
		// { x: 1024, y: 576 }
	}

}

export const canvas = Canvas.newCanvas()