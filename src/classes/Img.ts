export class Img extends Image {
	public constructor(src?: string, width?: number, height?: number) {
		super(width, height)
		if (src) { this.src = src }
	}
}
