class Events {

	static #instance: Events
	#lastKeyPressed
	#keys: { [key: string]: { pressed: boolean } }

	/***** Getter *****/

	/**
	 * @returns a string that represents the last key pressed
	 */
	get lastKeyPressed() { return this.#lastKeyPressed }
	/**
	 * @returns an object containing the following properties:
	 * - w: an object that has a boolean "pressed" property 
	 * - a: an object that has a boolean "pressed" property 
	 * - s: an object that has a boolean "pressed" property 
	 * - d: an object that has a boolean "pressed" property
	 */
	get keys() { return this.#keys }

	/**
	 * This method initiates the singleton Events object 
	 * @returns the Events object
	 */
	public static newEvents() {
		if (!this.#instance) { return new Events() }
		return this.#instance
	}

	private constructor() {
		this.#lastKeyPressed = ''
		this.#keys = {
			w: { pressed: false },
			a: { pressed: false },
			s: { pressed: false },
			d: { pressed: false }
		}
	}

	/**
	 * This method adds the events on the initial game start
	 */
	public initMainEvents() {
		addEventListener('keydown', e => {
			switch (e.key) {
				case 'w':
				case 'W':
				case 'ArrowUp':
					console.log('up')
					this.#keys.w.pressed = true
					this.#lastKeyPressed = 'w'
					break

				case 'a':
				case 'A':
				case 'ArrowLeft':
					console.log('left')
					this.#keys.a.pressed = true
					this.#lastKeyPressed = 'a'
					break

				case 's':
				case 'S':
				case 'ArrowDown':
					console.log('down')
					this.#keys.s.pressed = true
					this.#lastKeyPressed = 's'
					break

				case 'd':
				case 'D':
				case 'ArrowRight':
					console.log('right')
					this.#keys.d.pressed = true
					this.#lastKeyPressed = 'd'
					break

				case 'Shift':
					e.preventDefault()
					// player.run()
					break

				default:
					break
			}
		})
		addEventListener('keyup', e => {
			switch (e.key) {
				case 'w':
				case 'W':
				case 'ArrowUp':
					this.#keys.w.pressed = false
					break

				case 'a':
				case 'A':
				case 'ArrowLeft':
					this.#keys.a.pressed = false
					break

				case 's':
				case 'S':
				case 'ArrowDown':
					this.#keys.s.pressed = false
					break

				case 'd':
				case 'D':
				case 'ArrowRight':
					this.#keys.d.pressed = false
					break

				default:
					break
			}
		})
	}

}

export const events = Events.newEvents()
