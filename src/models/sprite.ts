export interface ISpriteArgs {
	image: IImageSrc
	position: IPosition
	frames?: {
		max: number
		hold: number
	}
	sprites?: ISpriteDirections
	animate?: boolean
	rotation?: number
}

interface IImageSrc {
	src: string
}

export interface IPosition {
	x: number
	y: number
}

type ISpriteDirections = {
	[key in 'up' | 'right' | 'down' | 'left']: IImageSrc
}
