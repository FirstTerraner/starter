export interface IMoveParams {
	moving: boolean
	animationID?: number
	inHouse?: boolean
}