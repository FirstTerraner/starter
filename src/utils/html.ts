export function assertNonNullish<T>(v: T, msg: string): asserts v is NonNullable<T> {
	if (v === null || v === undefined) { throw Error(msg) }
}


export function getElByQuery<T extends HTMLElement>(query: string) {
	const el = document.querySelector<T>(query)
	assertNonNullish(el, `Element "${query}" not found!`)
	if (!(el instanceof HTMLElement)) { throw new Error(`HTML ELement '${query}' has the wrong type`) }
	return el
}
