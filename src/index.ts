import { Player } from "./classes/Player"
import { events } from "./classes/Events"
import { getMap } from "./classes/Movables"

console.log('hello world!')

export const player = Player.newPlayer()

events.initMainEvents()

getMap().animate()